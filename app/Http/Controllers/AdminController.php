<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
//     public function index(){
//         return 
//            view('admin.index');
        
//    }

   public function edit(){
       return 
          view('admin.edit');
       
   }

   public function create(){
    return 
       view('admin.create');
    
    }

    public function pending(){
        return 
           view('admin.pending');
        
    }

    public function approved(){
        return 
           view('admin.approved');
        
    }
    public function index(){
        return 
           view('admin.index');
        
    }

}
