<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student; 


class UserController extends Controller
{
    public function index(){
   
         $students = Student::where('Status', '=', '0')
         ->orderBy('created_at','desc')
         ->paginate(8);
 
         return view('user.index', compact('students'));
            
         
    }

    public function edit(){
        return 
           view('user.edit');
        
   }
}
