<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SearchStudent extends Component
{
    public function render()
    {
        return view('livewire.search-student');
    }
}
