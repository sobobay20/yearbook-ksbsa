<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $table = 'students';
    protected $fillable = [
            'StudentNo',
            'StudentEmail',
            'StudentPassword',
            'StudentFirstName' ,
            'StudentOtherNames',
            'StudentNickname',
            'StudentBio',
            'StudentImage',
            'Department',
            'YearCompleted'
    ];

    public function getFullName()
   {
      return "{$this->StudentFirstName} {$this->StudentOtherNames}";
   }
}
