<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


class StudentFactory extends Factory
{
    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      
        return [
            'StudentNo' => $this->faker->unique()->numerify('########'),
            'StudentEmail' => $this->faker->safeEmail(),
            'StudentPassword' => $this->faker->password(),
            'StudentFirstName' => $this->faker->lastName(),
            'StudentOtherNames' => $this->faker->firstName(),
            'StudentNickname' => $this->faker->lastName(),
            'StudentBio' => $this->faker->text(),
            'StudentImage' => $this->faker->image('storage/app/public/images',640,480, null, false),
            'Department' => $this->faker->randomElement(['Accounting','Human Resource', 'Marketing','Supply Chain']),
            'YearCompleted' => $this->faker->year('+10 years')
    
        ];
    }
}

