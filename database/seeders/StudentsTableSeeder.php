<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
// use HasFactory;
// use App\Models\Student;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \App\Models\Student::factory(100)->create();
    }
}
