<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('StudentId');
            $table->string('StudentNo');
            $table->string('StudentEmail');
            $table->string('StudentPassword');
            $table->string('StudentFirstName');
            $table->string('StudentOtherNames');
            $table->string('StudentNickname');
            $table->string('StudentBio');
            $table->string('StudentImage');
            $table->string('YearCompleted');
            $table->string('Department');
            $table->string('Status')->default("0");
            $table->string('IdDeleted', 1)->default("0");
            $table->timestamp('EmailVerifiedAt')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
