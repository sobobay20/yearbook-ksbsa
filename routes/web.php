<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('app');
});


Route::group(['prefix' => 'user'], function(){
  Route::get('/',[\App\Http\Controllers\UserController::class,'index'])->name('user.index');
  Route::get('/edit',[\App\Http\Controllers\UserController::class,'edit'])->name('user.edit');
});

Route::group(['prefix' => 'admin'], function(){
    Route::get('/',[\App\Http\Controllers\AdminController::class,'index'])->name('admin.index');
    Route::get('/edit',[\App\Http\Controllers\AdminController::class,'edit'])->name('admin.edit');
    Route::get('/create',[\App\Http\Controllers\AdminController::class,'create'])->name('admin.create');
    Route::get('/pending',[\App\Http\Controllers\AdminController::class,'pending'])->name('admin.pending');
    Route::get('/approved',[\App\Http\Controllers\AdminController::class,'approved'])->name('admin.approved');
   
});