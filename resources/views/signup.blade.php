<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'KSBSA- Year Book') }}</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('fonts/material-icon/css/material-design-iconic-font.min.css') }}">

    <!-- Main css -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('css/account/style.css') }}">
</head>
<body>
    <div class="main">
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <form method="post" id="signup-form" class="signup-form" action="">
                        <h2 class="form-title">Create account</h2>
            
                        <div class="form-group">
                            <input type="text" class="form-input" name="student_no" placeholder="Enter Your Student Number" required/>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-input" name="user_email" placeholder="Enter Your Email" required/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" name="user_password" id="password" placeholder="Enter Your Password" required/>
                            <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" placeholder="Confirm Your Password" required/>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="signup"  class="form-submit" value="Sign up"/>
                        </div>
                    </form>
                    <p class="loginhere">
                        Have already an account ? <a href="../login" class="loginhere-link">Login here</a>
                    </p>
                </div>
            </div>
        </section>

    </div>
    <!-- JS -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/account/main.js') }}"></script>
</body><!-- This templates was made by Colorlib (https://colorli
    <script src="{{ asset('js/year.js') }}"></script>b.com) -->
</html>