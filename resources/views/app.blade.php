<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.partials.userHead')
    </head>
    <body>
        @include('layout.partials.userHeader')
        <main id="main">
            <section id="search" class="search-bg">
            <div class="container mt-5">
                   <div class="card">
                        <div class="card-header">
                           <h4 class="card-title">
                            <i class="bi bi-person"></i>
                               Profile Settings
                           </h3>
                        </div>
                       <div class="card-body">
                       <div class="row mt-2 p-5">
                        <div class="col-md-4">
                            <div class="image_area">      
                                <label for="upload_image">
                                    <img src="{{ asset('images/team/team-2.jpg') }}" id="uploaded_image" class="img-responsive img-circle" />
                                    <div class="overlay">
                                        <div class="text">Change Profile Picture</div>
                                    </div>
                                    <input type="file" name="user_image" class="image" id="upload_image" style="display:none" />
                                </label>
    
                            </div>  
                        </div>
                        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"  aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                  <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Crop Image Before Upload</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bi bi-x"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="img-container">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <img src="" id="sample_image" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="preview"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="crop" class="btn btn-primary">Crop</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        </div>
                                  </div>
                                </div>
                                </div>
    
                        <div class="col-md-4">
                            <div class="form-group">
                            <label for="">First Name</label>
                            <input type="text" class="form-control" value="Sadicka" name=""/>
                            </div>
                            <div class="form-group">
                            <label for="">Other Names</label>
                            <input type="text" class="form-control" value="Sessah" name=""/>
                            </div>
                            <div class="form-group">
                            <label for="">Year Completed</label>
                               <select class="form-control" name="year_completed" id="ddlYears">
                                 <option value="year">Select Year of Completion</option>
                                </select>
                            </div>
                            <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" class="form-control" value="sobobay20@gmail.com" name=""/>
                            </div>
                        </div>
    
                        <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Student Number</label>
                                <input type="text" class="form-control" value="20481524" name=""/>
                            </div>
                            <div class="form-group">
                            <label for="">Nickname</label>
                                <input type="text" class="form-control" value="Saddie" name=""/>
                            </div>
                            <div class="form-group">
                            <label for="">Department</label>
                                <select class="form-control" name="department">
                                    <option value="year">Select Department</option>
                                    <option value="year">Accounting & Finance</option>
                                    <option value="year">Human Resource & Organizational Development </option>
                                    <option value="year">Marketing & Corporate Strategy</option>
                                    <option value="year">Supply Chain & Information Systems</option>
                                </select>
                                </div>
                
                            <div class="form-group">
                            <label for="">Bio</label>
                                <textarea class="form-control" name="user_bio"  cols="2" rows="2">A KIS Lady</textarea>
                            </div>
    
                            <div class="form-group">
                                <input type="submit" name="signup"  class="form-submit" value="Update Profile"/>
                                </div>
                        </div>
    
                    </div>
                       </div>
                   </div>
                   
            </div>
            </section>
        </main>

        @include('layout.partials.userFooter')
        @include('layout.partials.userScripts')
   
    </body>
</html>