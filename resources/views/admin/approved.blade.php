@extends('layout.partials.admin')

@section('content')
<div class="page-wrapper" style="min-height: 250px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class=" card white-box">
                    <div class="card-heading">
                        <h2>Approved List of Users</h2>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-responsive-lg" id="datatable">
                                <thead>
                                    <tr>
                                        <th class="border-top-0"></th>
                                        <th class="border-top-0">Full Name</th>
                                        <th class="border-top-0">Nickname</th>
                                        <th class="border-top-0">Year</th>
                                        <th class="border-top-0">Department</th>
                                        <th class="border-top-0">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Evans Amoah</td>
                                        <td>None</td>
                                        <td>2016</td>
                                        <td>Accounting & Finance</td>
                                        <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Edmund Ofori</td>
                                        <td>None</td>
                                        <td>2020</td>
                                        <td>Human Resources & Organisational Development</td>
                                        <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Christa Agyemang</td>
                                        <td>Oye</td>
                                        <td>2019</td>
                                        <td>Marketing & Corporate Strategy</td>
                                         <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Perry Bona</td>
                                        <td>None</td>
                                        <td>2018</td>
                                        <td>Supply Chain & Information Systems</td>
                                         <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Evans Amoah</td>
                                        <td>None</td>
                                        <td>2017</td>
                                        <td>Accounting & Finance</td>
                                        <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Edmund Ofori</td>
                                        <td>None</td>
                                        <td>2016</td>
                                        <td>Human Resources & Organisational Development</td>
                                        <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Christa Agyemang</td>
                                        <td>Oye</td>
                                        <td>2015</td>
                                        <td>Marketing & Corporate Strategy</td>
                                         <td>
                                             <a href="" class="p-2">
                                               <i class="fas fa-edit text-primary"></i>
                                             </a>
                                             <a href="" class="p-2">
                                                <i class="fas fa-trash text-danger"></i>
                                             </a>
                                             <a href="" class="p-2">
                                                <i class="fas fa-undo text-secondary"></i>
                                             </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Perry Bona</td>
                                        <td>None</td>
                                        <td>2014</td>
                                        <td>Supply Chain & Information Systems</td>
                                         <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Evans Amoah</td>
                                        <td>None</td>
                                        <td>2013</td>
                                        <td>Accounting & Finance</td>
                                        <td>
                                            <a href="" class="p-2">
                                                <i class="fas fa-edit text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-trash text-danger"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-undo text-secondary"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Edmund Ofori</td>
                                        <td>None</td>
                                        <td>2012</td>
                                        <td>Human Resources & Organisational Development</td>
                                        <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Christa Agyemang</td>
                                        <td>Oye</td>
                                        <td>2011</td>
                                        <td>Marketing & Corporate Strategy</td>
                                         <td>
                                             <a href="" class="p-2">
                                            <i class="fas fa-edit text-primary"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="" class="p-2">
                                            <i class="fas fa-undo text-secondary"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-2.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Perry Bona</td>
                                        <td>None</td>
                                        <td>2011</td>
                                        <td>Supply Chain & Information Systems</td>
                                         <td>
                                            <a href="" class="p-2">
                                                <i class="fas fa-edit text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-trash text-danger"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-undo text-secondary"></i>
                                            </a>
                                        </td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>
@endsection

@section('custom_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    });
</script>
@endsection
