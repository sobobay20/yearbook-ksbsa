@extends('layout.partials.admin')

@section('content')
<div class="page-wrapper" style="min-height: 250px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class=" card white-box">
                    <div class="card-heading">
                        <h2>Pending Approval List of Users</h2>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-responsive-lg" id="datatable">
                                <thead>
                                    <tr>
                                        <th class="border-top-0"></th>
                                        <th class="border-top-0">Full Name</th>
                                        <th class="border-top-0">Nickname</th>
                                        <th class="border-top-0">Year</th>
                                        <th class="border-top-0">Department</th>
                                        <th class="border-top-0">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Evans Amoah</td>
                                        <td>None</td>
                                        <td>2016</td>
                                        <td>Accounting & Finance</td>
                                        <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Edmund Ofori</td>
                                        <td>None</td>
                                        <td>2020</td>
                                        <td>Human Resources & Organisational Development</td>
                                        <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Christa Agyemang</td>
                                        <td>Oye</td>
                                        <td>2019</td>
                                        <td>Marketing & Corporate Strategy</td>
                                         <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Perry Bona</td>
                                        <td>None</td>
                                        <td>2018</td>
                                        <td>Supply Chain & Information Systems</td>
                                         <td>
                                            <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Evans Amoah</td>
                                        <td>None</td>
                                        <td>2017</td>
                                        <td>Accounting & Finance</td>
                                        <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Edmund Ofori</td>
                                        <td>None</td>
                                        <td>2016</td>
                                        <td>Human Resources & Organisational Development</td>
                                        <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Christa Agyemang</td>
                                        <td>Oye</td>
                                        <td>2015</td>
                                        <td>Marketing & Corporate Strategy</td>
                                         <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Perry Bona</td>
                                        <td>None</td>
                                        <td>2014</td>
                                        <td>Supply Chain & Information Systems</td>
                                         <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Evans Amoah</td>
                                        <td>None</td>
                                        <td>2013</td>
                                        <td>Accounting & Finance</td>
                                        <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Edmund Ofori</td>
                                        <td>None</td>
                                        <td>2012</td>
                                        <td>Human Resources & Organisational Development</td>
                                        <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Christa Agyemang</td>
                                        <td>Oye</td>
                                        <td>2011</td>
                                        <td>Marketing & Corporate Strategy</td>
                                         <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="" class="d-flex align-items-center"><img
                                                    src="{{ asset('images/team/team-1.jpg') }}" alt="user-img" class="img-circle" width="60" style="border-radius: 50%;">
                                                </a>
                                            </div>
                                        </td>
                                        <td>Perry Bona</td>
                                        <td>None</td>
                                        <td>2011</td>
                                        <td>Supply Chain & Information Systems</td>
                                         <td>
                                           <a href="" data-toggle="modal" data-target="#viewUser" class="p-2">
                                                <i class="fas fa-eye text-primary"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-check text-success"></i>
                                            </a>
                                            <a href="" class="p-2">
                                                <i class="fas fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div>

                    <!-- Modal -->
                    <div class="modal fade" id="viewUser" tabindex="-1" role="dialog" aria-labelledby="viewUser" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header theme-bg">
                            <h3 class="modal-title text-white" id="viewUser">User Profile Overview</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">                            
                            <div class="card shadow">
                              <div class="card-body">
                                <!-- Comment Row -->
                                <div class="d-flex flex-row comment-row p-3">
                                    <div class="p-2"><img src="{{ asset('images/team/team-2.jpg') }}" alt="user" width="150" height="150" class="rounded-circle">
                                    </div>
                                    <div class="row">
                                       <div class="col-md-6 col-sm-12">
                                          <div class="comment-text ps-2 ps-md-3 w-100">
                                            <strong>Full Name:</strong> 
                                            <h5 class="font-medium">Christa Agyemang</h5>
    
                                            <strong>Email:</strong> 
                                            <h5 class="font-medium">kristaoye@gmail.com</h5>
    
                                            <strong>Year of Completion:</strong> 
                                            <h5 class="font-medium">2019</h5>
    
                                            <strong>User Bio:</strong> 
                                            <span class="mb-3 d-block">Lorem Ipsum is simply dummy text of the printing and type setting industry.It has survived not only five centuries. </span>
                                        
                                          </div>
                                        </div>
    
                                        <div class="col-md-6 col-sm-12">
                                            <div class="comment-text ps-2 ps-md-3 w-100">
                                                <strong>Nickname:</strong> 
                                                <h5 class="font-medium">Krista Oye</h5>
    
                                                <strong>Student Number:</strong> 
                                                <h5 class="font-medium">30465276</h5>
    
                                                <strong>Department:</strong> 
                                                <h5 class="font-medium">Accounting $ Finance</h5>
                                        
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                                 <!-- Comment Row -->
                              </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div> -->
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    });
</script>
@endsection
