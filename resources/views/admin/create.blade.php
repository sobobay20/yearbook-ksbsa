@extends('layout.partials.admin')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
       <div class="card">
           <div class="card-heading">
              <h2 class="box-title">Student Information</h2>
           </div>
           <div class="card-body">
            <form class="form-horizontal form-material" method="post" action="" enctype="multipart/form-data">
              <div class="row">
                <!-- Column -->
                <div class="col-lg-4 col-xlg-3 col-md-12 imgUp">       
                    <div class="imagePreview"></div>
                    <label style="width: 80%;" class="text-center btn btn-primary button mt-2">
                        Upload Profile Picture
                    <input type="file" name="StudentImage" class="uploadFile img" style="display:none; overflow: hidden;">
                    </label>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-8 col-xlg-9 col-md-12">
                  <div class="row">
                      <div class="col-lg-6">
                            <div class="form-group mb-4">
                                <label class="col-md-12 p-0">Student Number</label>
                                <div class="col-md-12 border-bottom p-0">
                                    <input type="text" name="StudentNo"
                                        class="form-control p-0 border-0"> </div>
                            </div>

                            <div class="form-group mb-4">
                                <label class="col-md-12 p-0">First Name</label>
                                <div class="col-md-12 border-bottom p-0">
                                    <input type="text" name="StudentFirstName"
                                        class="form-control p-0 border-0"> </div>
                            </div>

                            <div class="form-group mb-4">
                                <label class="col-md-12 p-0">Student Email</label>
                                <div class="col-md-12 border-bottom p-0">
                                    <input type="text" name="StudentEmail"
                                        class="form-control p-0 border-0">
                                </div>
                            </div>
                          
                            <div class="form-group mb-4">
                                <label class="col-sm-12">Year of Completion</label>

                                <div class="col-sm-12 border-bottom">
                                    <select name="YearCompleted" id="ddlYears" class="form-select shadow-none p-0 border-0 form-control-line">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group mb-4">
                            <label class="col-sm-12">Department</label>

                            <div class="col-sm-12 border-bottom">
                                <select name="Department" class="form-select shadow-none p-0 border-0 form-control-line">
                                    <option value="0"></option>
                                    <option value="ACF">Accounting & Finance</option>
                                    <option value="HROD">Human Resources & Organisational Development</option>
                                    <option value="MCS">Marketing & Corporate Strategy</option>
                                    <option value="SCIS">Supply Chain & Information Systems</option>
                                </select>
                            </div>
                        </div>
                           
                      </div>        
                      <div class="col-lg-6">
                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Student Nickname</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="text" name="StudentNickname"
                                    class="form-control p-0 border-0"> </div>
                        </div>

                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Other Names</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="text" name="StudentOtherNames"
                                    class="form-control p-0 border-0"> </div>
                        </div>

                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Password</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="text" name="StudentPassword"
                                    class="form-control p-0 border-0"> 
                            </div>
                        </div>

                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Confirm Password</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="text" name="ConfirmPassword"
                                    class="form-control p-0 border-0"> 
                            </div>
                        </div>

                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Bio</label>
                            <div class="col-md-12 border-bottom p-0">
                                <textarea name="StudentBio" rows="2" class="form-control p-0 border-0"></textarea>
                            </div>
                        </div>

                       
                  </div>

                  <div class="form-group mb-4">
                    <div class="col-sm-12">
                        <button name="add_user" type="submit" class="btn btn-primary button float-right">Add User</button>
                    </div>
                  </div>
            </div>
       </div>
              </div>
            </form>
           </div>
        </div>
   </div>
       
</div>
@endsection

@section('custom_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    });
</script>
@endsection
