<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.partials.appHead')
    
</head>

<body>
    @include('layout.partials.appHeader')
        <section id="hero">
            <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
                    <div class="carousel-inner" role="listbox">
                        <!-- Slide 1 -->
                        <div class="carousel-item active" style="background-image: url({{ asset('images/slide/slide3.jpg') }})">
                            <div class="carousel-container">
                                <div class="container">
                                <h2 class="animate__animated animate__fadeInDown">Welcome to <span>Green</span></h2>
                                    <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide 2 -->
                        <div class="carousel-item" style="background-image: url({{ asset('images/slide/slide2.jpg') }})">
                            <div class="carousel-container">
                                <div class="container">
                                <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
                                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                                </div>
                            </div>
                        </div>

                        <!-- Slide 3 -->
                        <div class="carousel-item" style="background-image: url({{ asset('images/slide/slide1.jpg') }})">
                            <div class="carousel-container">
                                <div class="container">
                                <h2 class="animate__animated animate__fadeInDown">Sequi ea ut et est quaerat</h2>
                                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                                </div>
                            </div>
                        </div>

                    </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
            </a>

            </div>
    </section>

    <main id="main">
        <section id="search" class="search-bg1">
        <div class="container mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-md-10">
                    <div class="card p-3 py-4">
                        <div class="row g-3 mt-2">
                        <div class="col-md-2">
                            <select name="year" id="ddlYears" class="form-control">
                            <option value="0">Year</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="department" id="department" class="form-control">
                                <option value="0">Department</option>
                                <option value="ACF">Accounting & Finance</option>
                                <option value="HROD">Human Resources & Organisational Development</option>
                                <option value="MCS">Marketing & Corporate Strategy</option>
                                <option value="SCIS">Supply Chain & Information Systems</option>
                            </select>    
                        </div>
                        <div class="col-md-6"> <input type="search" placeholder="Enter Full Name" class="form-control" >
                        </div>
                        <div class="col-md-2"> <button class="btn btn-secondary btn-block">Search Results</button> 
                        </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        </section>
        <section id="team" class="team section-bg">
        <div class="container">
            <div class="section-title">
            <h2>ALUMNI</h2>
            <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
            </div>
            <div class="row">
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="{{ asset('images/team/team-2.jpg') }}" alt="">
                <h4>Walter White</h4>
                <p>
                    Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui aut aut aut
                </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="{{ asset('images/team/team-1.jpg') }}" alt="">
                <h4>Sarah Jhinson</h4>
                <p>
                    Repellat fugiat adipisci nemo illum nesciunt voluptas repellendus. In architecto rerum rerum temporibus
                </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="{{ asset('images/team/team-2.jpg') }}" alt="">
                <h4>William Anderson</h4>
                <p>
                    Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro et laborum toro des clara
                </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member">
                <img src="{{ asset('images/team/team-3.jpg') }}" alt="">
                <h4>William Anderson</h4>
                <p>
                    Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro et laborum toro des clara
                </p>
                </div>
            </div>
            </div>
        </div>
        </section>
    </main>
    @include('layout.partials.appFooter')
    @include('layout.partials.appScripts')
     @yield('custom_js')
    </script>
    </body>
</html>