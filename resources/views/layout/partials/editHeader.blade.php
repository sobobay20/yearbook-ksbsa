
 <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center">
    <a href="main.php" class="logo me-auto"><img src="{{ asset('images/logo.png') }}" alt="" class="img-fluid"></a>
    <nav id="navbar" class="navbar">
        <ul>
            <li>
                <div class="dropdown">
                <a style="cursor: pointer;" class="dropdown" data-toggle="dropdown">
                    <img src="{{ asset('images/team/team-2.jpg') }}" style="border-radius: 50%;" alt="user-img" width="36" class="img-circle">
                    <span style="color: #706e6e" class="p-2"> Welcome, Steave</span>
                </a>
                <ul class="dropdown-menu p-3" style="cursor: pointer;" role="menu" aria-labelledby="menu1">
                    <a class="p-1" style="color: #706e6e" href="edit-user.php">
                        <li role="presentation"><i class="bi bi-person-fill"></i>
                            <span class="p-1">Profile</span>
                        </li>
                    </a>
                    <a class="p-1" style="color: #706e6e" href="logout.php">
                        <li role="presentation"><i class="bi bi-box-arrow-right"></i>
                            <span class="p-1">Logout</span>
                        </li>
                    </a>
                </ul>
                </div>
            </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
    </nav>
    </div>
</header>







