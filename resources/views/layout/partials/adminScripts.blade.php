
<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/admin/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/admin/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/admin/app-style-switcher.js') }}"></script>
    <script src="{{ asset('js/admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('js/admin/waves.js') }}"></script>
    <script src="{{ asset('js/admin/sidebarmenu.js') }}"></script>
    <script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.js') }}"></script>
    <script src="{{ asset('js/admin/plugins/bower_components/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('js/admin/plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('js/admin/pages/dashboards/dashboard1.js') }}"></script>   
    <script src="{{ asset('js/admin/jquery.datatables.min.js') }}"></script>
    <script src="{{ asset('js/admin/datatables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/admin/avartar.js') }}"></script>
    <script src="{{ asset('js/admin/custom.js') }}"></script> 
    <script src="{{ asset('js/img_crop.js') }}" defer></script> 
    <script src="{{ asset('js/years.js') }}" defer></script> 
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable();
        });
    </script>
   