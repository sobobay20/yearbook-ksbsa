<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.partials.editHead')
    
</head>
<body>
    @include('layout.partials.editHeader')
    @yield('content')
     
    @include('layout.partials.editFooter')
    @include('layout.partials.editScripts')

</html>