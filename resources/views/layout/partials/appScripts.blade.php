
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script> 
<script src="{{ asset('vendor/glightbox/js/glightbox.min.js') }}" defer></script> 
<script src="{{ asset('vendor/isotope-layout/isotope.pkgd.min.js') }}" defer></script> 
<script src="{{ asset('vendor/php-email-form/validate.js') }}" defer></script> 
<script src="{{ asset('vendor/swiper/swiper-bundle.min.js') }}" defer></script> 
<script src="{{ asset('js/user/main.js') }}" defer></script> 
<script src="{{ asset('js/years.js') }}" defer></script> 
