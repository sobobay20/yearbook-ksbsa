<header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center">
    <a href="index.html" class="logo me-auto"><img src="{{ asset('images/logo.png') }}" alt="" class="img-fluid"></a>
    <nav id="navbar" class="navbar">
        <ul>
            <li><a class="getstarted scrollto" href="login">Login</a></li>
            <li><a class="getstarted scrollto" href="signup">Sign Up</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
    </nav>
    </div>
</header>