<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.partials.userHead')
    @livewireStyles
</head>
<body>
    @include('layout.partials.userHeader')
    @yield('content')
     
    @include('layout.partials.userFooter')
    @include('layout.partials.userScripts')
    @livewireScripts
</body>
</html>