@extends('layouts.app')

@section('content')
<div class="main">
    <section class="signup">
        <div class="container">
            <div class="signup-content">
                <form method="post" id="signup-form" class="signup-form" action="">
                    <h2 class="form-title">Create account</h2>
        
                    <div class="form-group">
                        <input type="text" class="form-input" name="student_no" placeholder="Enter Your Student Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-input" name="user_email" placeholder="Enter Your Email" required/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-input" name="user_password" id="password" placeholder="Enter Your Password" required/>
                        <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-input" placeholder="Confirm Your Password" required/>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="signup"  class="form-submit" value="Sign up"/>
                    </div>
                </form>
                <p class="loginhere">
                    Have already an account ? <a href="../login" class="loginhere-link">Login here</a>
                </p>
            </div>
        </div>
    </section>

</div>
@endsection
