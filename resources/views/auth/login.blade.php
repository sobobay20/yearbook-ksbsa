@extends('layouts.partials.account')

@section('content')
<div class="main">

    <section class="signup">
        <!-- <img src="{{ asset('images/signup-bg.jpg') }}" alt=""> -->
        <div class="container">
            <div class="signup-content">
                <form method="POST" action="" id="signup-form" class="signup-form">
                    <h2 class="form-title">Sign In</h2>
                    <div class="form-group">
                        <input type="text" class="form-input" name="user_email" id="name" placeholder="Enter Your Email"/>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" class="form-input" name="user_password" id="password" placeholder="Enter Your Password"/>
                        <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                    </div>
                   
                    <div class="form-group">
                        <!-- <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" /> -->
                        <label for="agree-term" class="label-agree-term"><span><span></span></span><a href="#" class="term-service">Forgot Password? </a></label>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="login" id="submit" class="form-submit" value="Login"/>
                    </div>
                </form>
                <p class="loginhere">
                    New here ? <a href="../signup/" class="loginhere-link">Sign up here</a>
                </p>
            </div>
        </div>
    </section>

</div>
@endsection
